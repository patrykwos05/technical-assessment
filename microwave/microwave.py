import time
import logging
import socket
from threading import Thread

import microwave.auxiliary.config as cfg
from microwave.auxiliary.exceptions import NotConnected


class MicrowaveGenerator:
    logger_format = "[%(asctime)s]; %(threadName)s; %(levelname)s; %(message)s"

    def __init__(
        self,
        ip_address: str = cfg.TCP_TEST_ADDR,
        tcp_port: int = cfg.TCP_TEST_PORT,
        version: int = 0,
        name: str = "mw-0"
    ):
        self.ip_address: str = ip_address
        self.tcp_port: int = tcp_port
        self.version: int = version
        self.name: str = name
        self.tcp_client = None
        self.thread = None

        if self.version == 0:
            self.logger = logging.getLogger(name)
            self.logger.setLevel(logging.DEBUG)
        else:
            self.logger = logging.getLogger(name)
            self.logger.setLevel(logging.INFO)

        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter(self.logger_format))
        self.logger.addHandler(handler)

        self.connect()

    def connect(self):
        con_attempt = 1
        while True:
            try:
                # try to connect
                self.tcp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.tcp_client.connect((self.ip_address, self.tcp_port))
                self.logger.debug("MicrowaveGenerator {}: connected to TCP server @{}:{}.".format(self.name, self.ip_address, self.tcp_port))
                break

            except ConnectionRefusedError:
                self.logger.debug("MicrowaveGenerator {}: failed connection attempt to TCP server. @{}:{}. Retrying...".format(self.name, self.ip_address, self.tcp_port))
                con_attempt += 1
                time.sleep(0.5)
                # raise NotConnected if is more than 5 attempts
                if con_attempt == 6:
                    self.logger.error("MicrowaveGenerator {}: unable to connect to host device: IP {} did not respond.".format(self.name, self.ip_address))
                    raise NotConnected

        self.start()

    def disconnect(self):
        self.tcp_client.close()
        self.thread.join()
        self.logger.info("Disconnected.")

    def send(self, command):
        self.tcp_client.send(command.encode())

    def start(self):
        self.thread = Thread(target=self.keepalive)
        self.thread.start()

    def keepalive(self) -> None:
        while True:
            try:
                self.send("{AZ001127;")
                self.logger.debug("MicrowaveGenerator {}: 0.5s timer at 0.5.".format(self.name))
                time.sleep(0.5)
            except OSError:
                break
