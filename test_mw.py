from microwave.microwave import MicrowaveGenerator
import time
from microwave.auxiliary.exceptions import NotConnected


def main():
    try:
        mw = MicrowaveGenerator("127.0.0.1", 60260, 0)
        start_time = time.time()
        print("Hello from main thread at 0")
        time.sleep(1)
        print("Hello from main thread at {}".format((time.time() - start_time)))
        time.sleep(2)
        print("Goodbye from main thread at {}".format((time.time() - start_time)))
    except NotConnected:
        print("Device not connected.")
    else:
        try:
            mw
        except NameError:
            pass
        else:
            mw.disconnect()


if __name__ == "__main__":
    main()
